function [restored_data, amp_values] = restoreData(sorted_data, packed_header, restored_data)

amp_values = getAmpValues();
tvg_ref = [500, 1000, 1700, 2600, 3600, 4800, 6200, 8000];
range_ref = [10, 25, 50, 100, 250, 500, 1000, 3000, 5000, 10000];
full_range = range_ref + range_ref .* 0.2;


for idx = 1:length(sorted_data)

    %textprogressbar( idx / length(sorted_data) * 100 );
    
    current_gain = [packed_header(idx).wd_high_gain, packed_header(idx).wd_low_gain,...
                    packed_header(idx).dd_high_gain, packed_header(idx).dd_low_gain];
    
    current_tvg = [packed_header(idx).wd_high_tvg, packed_header(idx).wd_low_tvg, ...
                   packed_header(idx).dd_high_tvg, packed_header(idx).dd_low_tvg];

    x = [sorted_data(idx).wd_high.data; sorted_data(idx).wd_low.data; ...
         sorted_data(idx).dd_high.data; sorted_data(idx).dd_low.data]';
    
    piece = zeros(packed_header(idx).data_count, 4);
    
    
    range_count = linspace(0, packed_header(idx).range + packed_header(idx).range * 0.2, packed_header(idx).data_count);
    valid_idx = find(sum(x));
    %tvg_curve = 1 - exp(-(range_count * 50) ./ tvg_ref(current_tvg + 1)')';
    %tvg_curve = exp(-(range_count .* 50) ./ tvg_ref(current_tvg + 1)')';
    


    %piece(1:packed_header(idx).data_count, valid_idx) = (x(1:packed_header(idx).data_count,valid_idx)...
    %                               ./ amp_values(current_gain(valid_idx) + 1) + tvg_curve(:,valid_idx) ) * amp_values(1); % gain step 0
    %piece(piece < 0) = 0;
    
    piece(1:packed_header(idx).data_count, valid_idx) = x(1:packed_header(idx).data_count,valid_idx);
                                   %./ amp_values(current_gain(valid_idx) + 1) ) * amp_values(1); % gain step 0
    

    restored_data(idx).wd_high.data(1:packed_header(idx).data_count) = piece(:,1);
    restored_data(idx).wd_low.data(1:packed_header(idx).data_count) = piece(:,2);
    restored_data(idx).dd_high.data(1:packed_header(idx).data_count) = piece(:,3);
    restored_data(idx).dd_low.data(1:packed_header(idx).data_count) = piece(:,4);

    restored_data(idx).wd_high.pwd = sorted_data(idx).wd_high.pwd;
    restored_data(idx).wd_low.pwd = sorted_data(idx).wd_low.pwd;
    restored_data(idx).dd_high.pwd = sorted_data(idx).dd_high.pwd;
    restored_data(idx).dd_low.pwd = sorted_data(idx).dd_low.pwd;
    
    restored_data(idx).data_count = packed_header(idx).data_count;


end



end

