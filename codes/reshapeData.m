function [ total_imag ] = reshapeData(sorted_data, bathym_info)

%%
range_array = bathym_info.range_array;
range_list = bathym_info.range_list;
range_idx = bathym_info.range_idx;

total_imag = zeros(max(range_array), length(sorted_data), 4);
%textprogressbar('reshaping data: ');
for idx = 1:length(range_idx)
    %textprogressbar( idx / numel(range_idx)  * 100 );    
    
    part_range_idx = range_idx{idx};

    total_imag(:, part_range_idx, 1) ...
        = reshape([sorted_data(part_range_idx).dd_high_data], [max(range_list), numel(part_range_idx)]);

    total_imag(:, part_range_idx, 2) ...
        = reshape([sorted_data(part_range_idx).dd_low_data], [max(range_list), numel(part_range_idx)]);

    total_imag(:, part_range_idx, 3) ...
        = reshape([sorted_data(part_range_idx).wd_high_data], [max(range_list), numel(part_range_idx)]);

    total_imag(:, part_range_idx, 4) ...
        = reshape([sorted_data(part_range_idx).wd_low_data], [max(range_list), numel(part_range_idx)]);


end
%textprogressbar(' done');


end

