function [d] = gainData(sorted_data, packed_header, gain_info)

max_data_count = size(sorted_data(1).wd_high.data, 2);
data_length = length(sorted_data);

fields = struct('data', zeros(max_data_count, 1), 'pwd', 0, 'gain', 0, 'tvg', 0);
 
d = struct('wd_high', fields, 'wd_low', fields, ...
           'dd_high', fields, 'dd_low', fields, ...
           'data_count', 0);
 
d = repmat(d, [data_length, 1]);

[restored_data, amp_values]= restoreData(sorted_data, packed_header, d);

d(1).wd_high.gain = packed_header(1).wd_high_gain;
d(1).wd_low.gain = packed_header(1).wd_low_gain;
d(1).dd_high.gain = packed_header(1).dd_high_gain;
d(1).dd_low.gain = packed_header(1).dd_low_gain;

%d(1).wd_high.pwd = restored_data(1).wd_high.pwd;
%d(1).wd_low.pwd = restored_data(1).wd_low.pwd;
%d(1).dd_high.pwd = restored_data(1).dd_high.pwd;
%d(1).dd_low.pwd = restored_data(1).dd_low.pwd;

%d(1).wd_high.data = restored_data(1).wd_high.data .* amp_values(restored_data(1).wd_high.gain + 1);
%d(1).wd_low.data = restored_data(1).wd_low.data .* amp_values(restored_data(1).wd_low.gain + 1);
%d(1).dd_high.data = restored_data(1).dd_high.data .* amp_values(restored_data(1).dd_high.gain + 1);
%d(1).dd_low.data = restored_data(1).dd_low.data .* amp_values(restored_data(1).dd_low.gain + 1);

for idx = 1:length(restored_data)
d(idx).wd_high.data = restored_data(idx).wd_high.data;
d(idx).wd_low.data = restored_data(idx).wd_low.data;
d(idx).dd_high.data = restored_data(idx).dd_high.data;
d(idx).dd_low.data = restored_data(idx).dd_low.data;

d(idx).wd_high.data(d(idx).wd_high.data>32767) = 32767;
d(idx).wd_low.data(d(idx).wd_low.data>32767) = 32767;
d(idx).dd_high.data(d(idx).dd_high.data>32767) = 32767;
d(idx).dd_low.data(d(idx).dd_low.data>32767) = 32767;

d(idx).data_count = packed_header(idx).data_count;

end


% 
% next_step = zeros(1, 4);
% textprogressbar('gaining data: ');
% for idx = 1:data_length - 1
%     textprogressbar( idx / (data_length - 1) * 100 );
%     d(idx+1).data_count = packed_header(idx+1).data_count;
%     x = [d(idx).wd_high.data d(idx).wd_low.data ...
%          d(idx).dd_high.data d(idx).dd_low.data];
%      
%     current_gain = [d(idx).wd_high.gain d(idx).wd_low.gain ...
%                     d(idx).dd_high.gain d(idx).dd_low.gain];
%                     
%     N = packed_header(idx).data_count;
%     
%     valid_count = sum(x > max(x) * 0.7);
%     
%     energy = sum(x(1:N,:).^2);
%     beam_power = 10*log10(energy./valid_count);
%     
%     for jdx = 1:length(beam_power)
%         
%         current_factor = amp_values(current_gain);
%         
%         step = current_gain(jdx);
%         output_power_Normal = 10.^(gain_info.gain_level/10);
%         
%         if( beam_power(jdx) > 0 && abs(gain_info.gain_level - beam_power(jdx)) > gain_info.gain_criterion )
%             
%             %K = sqrt( (output_power_Normal*N) ./ energy(jdx));
%             K = sqrt( (output_power_Normal*valid_count) ./ energy(jdx));
%             
%             %factor = current_factor(jdx) * K;
%             step = factor_to_step(K, current_factor(jdx), amp_values, current_gain(jdx));
%         end
%         
%         next_step(jdx) = step;
%         
%     end
%     d(idx + 1).wd_high.gain = next_step(1);
%     d(idx + 1).wd_low.gain = next_step(2);
%     d(idx + 1).dd_high.gain = next_step(3);
%     d(idx + 1).dd_low.gain = next_step(4);
%     
% %     d(idx + 1).wd_high.data = restored_data(idx + 1).wd_high.data .* amp_values(d(idx + 1).wd_high.gain);
% %     d(idx + 1).wd_low.data = restored_data(idx + 1).wd_low.data .* amp_values(d(idx + 1).wd_low.gain);
% %     d(idx + 1).dd_high.data = restored_data(idx + 1).dd_high.data .* amp_values(d(idx + 1).dd_high.gain);
% %     d(idx + 1).dd_low.data = restored_data(idx + 1).dd_low.data .* amp_values(d(idx + 1).dd_low.gain);
%     
%     d(idx + 1).wd_high.data(d(idx + 1).wd_high.data>32767) = 32767;
%     d(idx + 1).wd_low.data(d(idx + 1).wd_low.data>32767) = 32767;
%     d(idx + 1).dd_high.data(d(idx + 1).dd_high.data>32767) = 32767;
%     d(idx + 1).dd_low.data(d(idx + 1).dd_low.data>32767) = 32767;
%     
% %     d(idx + 1).wd_high.pwd = restored_data(idx + 1).wd_high.pwd;
% %     d(idx + 1).wd_low.pwd = restored_data(idx + 1).wd_low.pwd;
% %     d(idx + 1).dd_high.pwd = restored_data(idx + 1).dd_high.pwd;
% %     d(idx + 1).dd_low.pwd = restored_data(idx + 1).dd_low.pwd;
%     
% %     %% fill data
% %     d(idx).wd_high.data = sorted_data.wd_high_data{idx};
% %     d(idx).wd_low.data = sorted_data.wd_low_data{idx};
% %     d(idx).dd_high.data = sorted_data.dd_high_data{idx};
% %     d(idx).dd_low.data = sorted_data.dd_low_data{idx};
% %     
% %     d(idx).wd_high.gain = packed_header(idx).wd_high_gain;
% %     d(idx).wd_low.gain = packed_header(idx).wd_low_gain;
% %     d(idx).dd_high.gain = packed_header(idx).dd_high_gain;
% %     d(idx).dd_low.gain = packed_header(idx).dd_low_gain;
%     
    
    
    
    

end

