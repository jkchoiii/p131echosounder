function [next_step] = factor_to_step(K, current_factor, amp_values, current_gain)

factor = K * current_factor;
diff_gain_ratio = abs(factor - amp_values(current_gain));

if(K > 1)
    for idx=current_gain:length(amp_values)
        gain_cmp = abs(factor - amp_values(idx));
        
        if(gain_cmp <= diff_gain_ratio)
            next_step = idx;
            diff_gain_ratio = gain_cmp;
        else
            break;
        end
        
    end
    
else
    for idx = current_gain:-1:1
        gain_cmp = abs(factor - amp_values(idx));
        
        if(gain_cmp <= diff_gain_ratio)
            next_step = idx;
            diff_gain_ratio = gain_cmp;
        else
            break;
        end
        
    end
    
end


end

