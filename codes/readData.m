function [header, packed_data, data_length] = readData(file_path, total_size)


fid = fopen(file_path);


data_ptr = checkValidHeader(fid, total_size);

data_length = length(data_ptr);
disp([ 'data_length: ', num2str(data_length)])
fseek(fid, 0, 'bof');

prev_header = readHeader(fid, data_ptr(1));
fseek(fid, 0, 'bof');


%% initialization



%% read data
packed_data = repmat(...
    struct(...
    'data', struct('sensor_data1', [], 'sensor_data2', []), ...
    'ch_sel', struct('ch_sel1', 0, 'ch_sel2', 0), ...
    'gain', struct('wd_high_gain', 0, 'wd_low_gain', 0, 'dd_high_gain', 0, 'dd_low_gain', 0)...
    ),[data_length, 1]);

header = repmat(prev_header, [data_length, 1]);


for idx = 1 : data_length
    
    

    
    [header(idx), packed_data(idx)]= compareHeaderChange(fid, packed_data(idx), data_ptr(idx));
    
    bit = ones(header(idx).data_count * header(idx).beam_count, 1) * 32767;
    
    read_data = fread(fid, header(idx).data_count * header(idx).beam_count, 'short');
    read_data = bitand(read_data, bit, 'int16');
    read_data = reshape(read_data, [header(idx).data_count, header(idx).beam_count]);
    
    packed_data(idx).data.sensor_data1 = read_data(:,1);
    packed_data(idx).data.sensor_data2 = read_data(:,2);

    
    %[tmp, K1(idx), K2(idx)]= AGC(read_data, 75, size(read_data, 1));

end


fclose(fid);




end

