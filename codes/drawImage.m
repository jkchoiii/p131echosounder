function [imag_handler] = drawImage(total_signal, bathym_info, packed_header, file_name)

figure;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.94]);
suptitle(file_name);
imag_handler = [subplot(2,2,1), subplot(2,2,2), subplot(2,2,3), subplot(2,2,4)];


current_line_x = zeros(size(total_signal, 1), 1) + 0.5;
current_line_y = 1:numel(current_line_x);
title_string = ["DD HIGH DATA", "DD LOW DATA", "WD HIGH DATA", "WD LOW DATA"];

for idx = 1:size(total_signal, 3)
    imagesc(imag_handler(idx), total_signal(:,:,idx))
    imag_handler(idx).Title.String= title_string(idx);
    imag_handler(idx).XLabel.String = 'received data number';
    yticks(imag_handler(idx), linspace(0, size(total_signal(:,:,idx), 1), 5));
    yticklabels(imag_handler(idx), linspace(0, max([packed_header(:).range]), 5));
    imag_handler(idx).YLabel.String = 'range';
    colormap(imag_handler(idx), 'jet');
    
    hold(imag_handler(idx), 'on');
    for jdx = 1:numel(bathym_info.range_list)
        bathym_x = bathym_info.range_idx{jdx};
        bathym_y = bathym_info.range_list(jdx) * ones(size(bathym_x, 2), 1);
        %plot(imag_handler(idx), bathym_x, bathym_y, 'r', 'LineWidth', 3);
        scatter(imag_handler(idx), bathym_x, bathym_y, 'r', '+', 'LineWidth', 0.6);
    end
    
    
    plot(imag_handler(idx), current_line_x, current_line_y, 'y', 'LineWidth', 2);
    hold(imag_handler(idx), 'off');
    
end

%saveas(gcf, [file_name '_imag.fig']);

end

