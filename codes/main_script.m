
clc; close all; clear global;

%% file open
file_root = 'E:\최종권\200127_측심기 포항 데이터 분석\20201204_측심데이터';
file_list = dir(fullfile(file_root, '*.dat'));
exist = dir('E:\최종권\200127_측심기 포항 데이터 분석\20201204_측심데이터\img\*.png');
% 
% list_idx = [];
% for idx = 1:length(file_list)
%     for jdx = 1:length(exist)
%         if strcmp(file_list(idx).name(1:10), exist(jdx).name(1:10))
%             list_idx = [list_idx idx];
%             break;
%         end
%     end
%     
% end 
% new_idx = setdiff(1:length(file_list), list_idx);
% file_list = file_list(new_idx);

gain_info = [];
gain_info.gain_level = 100;
gain_info.gain_criterion = 3;

%file_list = file_list(64);

parfor idx = 1:length(file_list)
    %clearvars -except idx file_list gain_info file_root;

     parworkers(file_root, file_list(idx).name, file_list(idx).bytes,gain_info);

     %save([file_name '_gained_data.mat'], 'gained_data', '-v7.3', '-nocompression');
     %saveas(gcf, [file_name '.png']);
    %load 2020012120.dat_gained_data.mat;
    %[est_results] = depthEstimation(gained_data, file_name);
    %[~] = depthEstimation(gained_data, file_name, file_root);
    
    %extractRawData(packed_data, file_name);
    
end

 


%figure; 





% %%
% %[tvg_data] = tvgData(gained_data, packed_header);
% 
% %load 2020012202_bathym_info.mat
% %load 2020012202_packed_header.mat
% %load 2020012202_gained_data.mat
% 
% 
% %figure; plot([packed_header(:).wd_high_gain])
% %hold on; plot(extractfield([gained_data(:).wd_high], 'gain'));
% 
% %disp('reshaping restored data');
% %[restored_total_imag] = gainReshapeData(restored_data);
% %[imag_handler] = drawImage(restored_total_imag, bathym_info, packed_header, file_name);
% disp('reshaping gained data');
% [gained_total_imag] = gainReshapeData(gained_data);
% [imag_handler] = drawImage(gained_total_imag, bathym_info, packed_header, file_name);
% drawRealTime(gained_data, imag_handler, file_name);
% disp('reshaping raw data');
% [total_imag] = reshapeData(sorted_data, bathym_info);
% %[imag_handler] = drawImage(total_imag, bathym_info, packed_header, file_name);
% %drawRealTime(sorted_data, imag_handler, file_name);
% 
% sensor_idx = 4;
% rtotal_imag = imresize(total_imag(:,:,sensor_idx), 0.25, 'nearest');
% figure; imagesc(rtotal_imag); colormap('jet');
% rgained_total_imag = imresize(gained_total_imag(:,:,sensor_idx), 0.25, 'nearest');
% figure; imagesc(rgained_total_imag); colormap('jet');
% rrestored_total_imag = imresize(restored_total_imag(:,:,sensor_idx), 0.25, 'nearest');
% figure; imagesc(rrestored_total_imag); colormap('jet');
% 
% rtotal_imag = imresize(total_imag(:,:,sensor_idx), 0.25, 'cubic');
% figure; imagesc(rtotal_imag); colormap('jet');
% rgained_total_imag = imresize(gained_total_imag(:,:,sensor_idx), 0.25, 'cubic');
% figure; imagesc(rgained_total_imag); colormap('jet');
% rrestored_total_imag = imresize(restored_total_imag(:,:,sensor_idx), 0.25, 'cubic');
% figure; imagesc(rrestored_total_imag); colormap('jet');

disp('done')

%%
% 
% % fid = fopen('bin.bin', 'w');
% % fwrite(fid, raw_data, 'short')
% % fclose(fid);
% % 
% % fid = fopen('wd_high_gain.bin', 'w');
% % fwrite(fid, wd_high_gain, 'short')
% % fclose(fid);
% 
