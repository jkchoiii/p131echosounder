function [reflected_x, max_idx, echo_exist] = removeTransmitSource(smooth_x, window_size, draw)


reflected_x = zeros(length(smooth_x), 1);
max_val = 0;
non_update_count = 0;
stop_update = window_size;
max_idx = 1;
min_val = max(smooth_x);


for idx = 1:length(smooth_x)
    if smooth_x(idx) > max_val
        max_val = smooth_x(idx);
        max_idx = idx;
        non_update_count = 0;
    else
        non_update_count = non_update_count + 1;
        min_val = smooth_x(idx);
        min_idx = idx;
        if non_update_count >= stop_update
            break;
        end
    end
    
end
non_update_count = 0;
for idx = min_idx:length(smooth_x)
    if smooth_x(idx) < min_val
        min_val = smooth_x(idx);
        min_idx = idx;
        non_update_count = 0;
    else
        non_update_count = non_update_count + 1;
        if non_update_count >= stop_update
            break;
        end
        
    end
    
    
end

if sum(smooth_x(min_idx:end)>min_val*2) < fix(window_size/2)
    echo_exist = 0;
else
    echo_exist = 1;
    
end
    


%pulse_width_idx = max_idx + fix(window_size/2);
pulse_width_idx = min_idx;

reflected_x(pulse_width_idx + 1 : end) = smooth_x(pulse_width_idx + 1 : end);
% if draw
%     %figure;
%     subplot(2,1,1)
%     plot(smooth_x);
%     subplot(2,1,2)
%     plot(reflected_x./max(reflected_x))
%     drawnow;
% end

end

