function [a, r2] = lin_reg(x,y)
clear;
clc;
close all;

%y = [   1.8921    4.3945   10.3760   25.0244   56.1523  105.5908  134.2773  271.2674    ];
%x = [   0  10   20  30  40  50  60  70 ];
y = [134.2773  271.2674    ];
x = [60  70 ];

%y = [        25.0244   56.1523  105.5908  134.2773  271.2674    ];
%x = [       30  40  50  60  70 ];

%plotregression()
% 선형 최소제곱 회귀분석을 위한 코드
% 입력값은 데이터의 x와 y좌표값
% 출력값은 기울기와 결정계수

x = x'; y=y';   % 행벡터를 열벡터로 변환

n = length(x);  % 입력된 값의 갯수를 구함

sx = sum(x);sy = sum(y);   % 각각의 합을 구함
sxx = sum(x.*x); sxy = sum(x.*y); syy = sum(y.*y); % 각각의 제곱합을 구함

a(1) = (n*sxy - sx*sy) / (n*sxx - sx^2);    % 최소제곱 회귀분석
a(2) = sy/n - a(1)*sx/n;                    % 최소제곱 회귀분석

r2 = ((n*sxy-sx*sy)/(sqrt(n*sxx-sx^2)*sqrt(n*syy-sy^2)))^2   % 결정계수 계산

x_plot = linspace(min(x),max(x),2);         % 그래프를 그리기 위해 그래프용 x 범위를 정해줌
y_plot = a(1)*x_plot + a(2);                % 그래프를 그리기 위해 그래프용 y 범위를 정해줌

plot(x,y,'.')                               % 주어진 데이터 점들을 그래프상에 표현
hold on
plot(x_plot,y_plot)                         % 최소제곱 회귀분석으로 구해진 선형 그래프를 그림