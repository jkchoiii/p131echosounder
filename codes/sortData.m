function [sorted_struct, bathym_info] = sortData(packed_header, packed_data)

% 1,2 = sensor wd1, sensor wd2 <- ch1
% 3,4 = sensor dd1, sensor dd2 <- ch2

%%%%%%%%%%%%%%%%%%%
%   BITXOR TABLE
%
% CH   1  2  3  4
%    -------------
%  1 | 0  3  2  5
%  2 | 3  0  1  6
%  3 | 2  1  0  7
%  4 | 5  6  7  0
%
%%%%%%%%%%%%%%%%%%%

range_array = [packed_header(:).data_count];

range_list = unique(range_array);

for idx = 1:numel(range_list)
    range_idx(idx) = {find(range_list(idx) == range_array)};
end
fields = struct('data', zeros(1, max(range_list)), 'pwd', 0, 'gain', 0);
sorted_struct = struct('wd_high', fields, 'wd_low', fields, ...
                       'dd_high', fields, 'dd_low', fields);

sorted_struct = repmat(sorted_struct, [length(packed_data), 1]);

dd_high_cnt = 0;
dd_low_cnt = 0;
dd_high_ping = [];
dd_low_ping = [];
for idx = 1:numel(packed_data)
    

    ch_sel = bitxor(packed_header(idx).ch1_sel, packed_header(idx).ch2_sel);
    
    sorted_struct(idx).data_count = packed_header(idx).data_count;
    
    if(ch_sel == 3) % 1, 2
        sorted_struct(idx).wd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).wd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
        
        sorted_struct(idx).wd_high.pwd = packed_header(idx).wd_high_pwd;
        sorted_struct(idx).wd_low.pwd = packed_header(idx).wd_low_pwd;
                
        sorted_struct(idx).wd_high.gain = packed_header(idx).wd_high_gain;
        sorted_struct(idx).wd_low.gain = packed_header(idx).wd_low_gain;
        
    elseif(ch_sel == 2) % 1, 3
        dd_high_cnt = dd_high_cnt + 1;
        dd_high_ping = [dd_high_ping idx];
        sorted_struct(idx).wd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).dd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
        
        sorted_struct(idx).wd_high.pwd = packed_header(idx).wd_high_pwd;
        sorted_struct(idx).dd_high.pwd = packed_header(idx).dd_high_pwd;
        
        sorted_struct(idx).wd_high.gain = packed_header(idx).wd_high_gain;
        sorted_struct(idx).dd_high.gain = packed_header(idx).dd_high_gain;
        
    elseif(ch_sel == 5) % 1, 4
        dd_low_cnt = dd_low_cnt + 1;
        dd_low_ping = [dd_low_ping idx];
        
        sorted_struct(idx).wd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).dd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
        
        sorted_struct(idx).wd_high.pwd = packed_header(idx).wd_high_pwd;
        sorted_struct(idx).dd_low.pwd = packed_header(idx).dd_low_pwd;
        
        sorted_struct(idx).wd_high.gain = packed_header(idx).wd_high_gain;
        sorted_struct(idx).dd_low.gain = packed_header(idx).dd_low_gain;
        
    elseif(ch_sel == 1) % 2, 3
        dd_high_cnt = dd_high_cnt + 1;
        dd_high_ping = [dd_high_ping idx];
        sorted_struct(idx).wd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).dd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
        
        sorted_struct(idx).wd_low.pwd = packed_header(idx).wd_low_pwd;
        sorted_struct(idx).dd_high.pwd = packed_header(idx).dd_high_pwd;
        
        sorted_struct(idx).wd_low.gain = packed_header(idx).wd_low_gain;
        sorted_struct(idx).dd_high.gain = packed_header(idx).dd_high_gain;
        
    elseif(ch_sel == 6) % 2, 4
        dd_low_cnt = dd_low_cnt + 1;
        dd_low_ping = [dd_low_ping idx];
        
        sorted_struct(idx).wd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).dd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
        
        sorted_struct(idx).wd_low.pwd = packed_header(idx).wd_low_pwd;
        sorted_struct(idx).dd_low.pwd = packed_header(idx).dd_low_pwd;
        
        sorted_struct(idx).wd_low.gain = packed_header(idx).wd_low_gain;
        sorted_struct(idx).dd_low.gain = packed_header(idx).dd_low_gain;
        
    elseif(ch_sel == 7) % 3, 4
        dd_high_cnt = dd_high_cnt + 1;
        dd_high_ping = [dd_high_ping idx];
        dd_low_cnt = dd_low_cnt + 1;
        dd_low_ping = [dd_low_ping idx];
        
        sorted_struct(idx).dd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
        sorted_struct(idx).dd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data2;
                
        sorted_struct(idx).dd_high.pwd = packed_header(idx).dd_high_pwd;
        sorted_struct(idx).dd_low.pwd = packed_header(idx).dd_low_pwd;
        
        sorted_struct(idx).dd_high.gain = packed_header(idx).dd_high_gain;
        sorted_struct(idx).dd_low.gain = packed_header(idx).dd_low_gain;
        
    elseif(ch_sel == 0) % (1, 1), (2, 2), (3, 3), (4, 4)
        if(packed_header(idx).ch1_sel == 1)
            sorted_struct(idx).wd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
            
            sorted_struct(idx).wd_high.pwd = packed_header(idx).wd_high_pwd;
            sorted_struct(idx).wd_high.gain = packed_header(idx).wd_high_gain;

        elseif(packed_header(idx).ch1_sel == 2)
            sorted_struct(idx).wd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
            
            sorted_struct(idx).wd_low.pwd = packed_header(idx).wd_low_pwd;
            sorted_struct(idx).wd_low.gain = packed_header(idx).wd_low_gain;
            
            
        elseif(packed_header(idx).ch1_sel == 3)
            dd_high_cnt = dd_high_cnt + 1;
            dd_high_ping = [dd_high_ping idx];
            sorted_struct(idx).dd_high.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
            
            sorted_struct(idx).dd_high.pwd = packed_header(idx).dd_high_pwd;
            sorted_struct(idx).dd_high.gain = packed_header(idx).dd_high_gain;
        
            
        elseif(packed_header(idx).ch1_sel == 4)
            dd_low_cnt = dd_low_cnt + 1;
            dd_low_ping = [dd_low_ping idx];
            sorted_struct(idx).dd_low.data(1:packed_header(idx).data_count) = packed_data(idx).data.sensor_data1;
            
            sorted_struct(idx).dd_low.pwd = packed_header(idx).dd_low_pwd;
            sorted_struct(idx).dd_low.gain = packed_header(idx).dd_low_gain;
           
        end
    end
end


bathym_info = [];
bathym_info.range_array = range_array;
bathym_info.range_list = range_list;
bathym_info.range_idx = range_idx;

end

