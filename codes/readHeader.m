function [ header ] = readHeader( fid, data_ptr )

header = [];

fseek(fid, data_ptr, 'bof');
val = fread(fid, 1, 'int');
val = fread(fid, 1, 'int');
val = fread(fid, 1, 'int');
val = fread(fid, 1, 'int');
val = fread(fid, 1, 'int');

%header.burst_no = fread(fid, 1, 'int');
fread(fid, 1, 'int');
%fread(fid, 1, 'int');
header.range = fread(fid, 1, 'int');

val = fread(fid, 4, 'int');
val = fread(fid, 1, 'float');
val = fread(fid, 1, 'long');
val = fread(fid, 2, 'int');
val = fread(fid, 7, 'float');
val = fread(fid, 4, 'int');


header.wd_high_gain = fread(fid, 1, 'int');
header.wd_low_gain = fread(fid, 1, 'int');
header.wd_high_tvg = fread(fid, 1, 'int');
header.wd_low_tvg = fread(fid, 1, 'int');
header.wd_high_pwd = fread(fid, 1, 'int')/2;
header.wd_low_pwd = fread(fid, 1, 'int')/2;
header.wd_high_power = fread(fid, 1, 'int');
header.wd_low_power = fread(fid, 1, 'int');


header.dd_high_gain = fread(fid, 1, 'int');
header.dd_low_gain = fread(fid, 1, 'int');
header.dd_high_tvg = fread(fid, 1, 'int');
header.dd_low_tvg = fread(fid, 1, 'int');
header.dd_high_pwd = fread(fid, 1, 'int')/2;
header.dd_low_pwd = fread(fid, 1, 'int')/2;
header.dd_high_power = fread(fid, 1, 'int');
header.dd_low_power = fread(fid, 1, 'int');

header.ch1_sel = fread(fid, 1, 'int');
header.ch2_sel = fread(fid, 1, 'int');

val = fread(fid, 12, 'int');
val = fread(fid, 3, 'float');
val = fread(fid, 3, 'int');
val = fread(fid, 4, 'int');
val = fread(fid, 4, 'float');
val = fread(fid, 36, 'int');

header.data_count = fread(fid, 1, 'int');
header.beam_count = fread(fid, 1, 'int');



end

