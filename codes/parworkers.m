function parworkers(file_root, file_name, total_size, gain_info)

disp(file_name)

    file_path = fullfile(file_root, file_name);
    disp('read')
     [packed_header, packed_data, ~] = readData(file_path, total_size);
     disp('sort')
     [sorted_data, ~] = sortData(packed_header, packed_data);
     clearvars packed_data;
     disp('gain')
     [gained_data] = gainData(sorted_data, packed_header, gain_info);
     clearvars packed_header sorted_data;

     disp('reshape')
non_filtered_data = zeros(max([gained_data(:).data_count]), 4, length(gained_data));

for kdx = 1:length(gained_data)
    x = [gained_data(kdx).wd_high.data, gained_data(kdx).wd_low.data, ...
         gained_data(kdx).dd_high.data, gained_data(kdx).dd_low.data];
     
    valid_idx = find(sum(x));

    for jdx = 1:length(valid_idx)

        current_valid_idx = valid_idx(jdx);
        current_x = x(1:gained_data(kdx).data_count, current_valid_idx);
        current_x = current_x ./ max(current_x);

        non_filtered_data(1:gained_data(kdx).data_count,current_valid_idx, kdx) = current_x;
    end

end

figure;
imag_handler = [subplot(2,2,1), subplot(2,2,2), subplot(2,2,3), subplot(2,2,4)];

imagesc(imag_handler(1), squeeze(non_filtered_data(:,3,:))); 
imag_handler(1).Title.String= 'dd high';
imagesc(imag_handler(2), squeeze(non_filtered_data(:,4,:))); 
imag_handler(2).Title.String= 'dd low';
imagesc(imag_handler(3), squeeze(non_filtered_data(:,1,:))); 
imag_handler(3).Title.String= 'wd high';
imagesc(imag_handler(4), squeeze(non_filtered_data(:,2,:))); 
imag_handler(4).Title.String= 'wd low';

set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.94]);
suptitle([file_name ' total ping: ' num2str(length(gained_data))]);
saveas(gcf, fullfile('E:\최종권\200127_측심기 포항 데이터 분석\20201204_측심데이터\img\', [file_name '_raw_data2.png']));
     
close(gcf)
clearvars gained_data non_filtered_data x imag_handler current_x;
disp('done')
end

