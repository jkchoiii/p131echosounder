function [ xcorr_rest ] = xcorr_fft( x1, x2 )
%XCORR_FFT Calculates the crosscorrelation of the two input vectors
%   using the fft based method
%

if size(x1, 1) > size(x1, 2)
    x1 = x1';
end

if size(x2, 1) > size(x2, 2)
    x2 = x2';
end

% zero pad 
if (length(x1) < length(x2))
    x1_pad = [ zeros(1,length(x2)-1) x1 zeros(1,length(x2)-length(x1)) ];
    x2_pad = [ x2 zeros(1,length(x2)-1) ];
else
    x1_pad = [ zeros(1,length(x1)-1) x1 ];
    x2_pad = [ x2 zeros(1,length(x1)-length(x2)+length(x1)-1) ];
end
% calculate crosscorrelation
fft_x1 = fft(x1_pad);
fft_x2 = fft(x2_pad);
fft_xcorr_res = fft_x1.*conj(fft_x2);
xcorr_rest = ifft(fft_xcorr_res);


end
