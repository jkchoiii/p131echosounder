function [tvg_data] = tvgData(data, packed_header)

tvg_ref = [500, 1000, 1700, 2600, 3600, 4800, 6200, 8000];
range_ref = [10, 25, 50, 100, 250, 500, 1000, 3000, 5000, 10000];
    
full_range = range_ref + range_ref .* 0.2;

valid_idx = find(sum(x));

%piece(1:packed_header(idx).data_count, valid_idx) = (x(1:packed_header(idx).data_count,valid_idx)...
%                               ./ amp_values(current_gain(valid_idx) + 1) + tvg_curve(:,valid_idx) ) * amp_values(1); % gain step 0
%piece(piece < 0) = 0;

%%

pwd = [ packed_header(:).wd_high_pwd; packed_header(:).wd_low_pwd; ...
        packed_header(:).dd_high_pwd; packed_header(:).dd_low_pwd; ]';

correct_range = zeros(length(packed_header), 1);

for idx=1:length(packed_header)
    
    current_tvg = [packed_header(idx).wd_high_tvg, packed_header(idx).wd_low_tvg, ...
                   packed_header(idx).dd_high_tvg, packed_header(idx).dd_low_tvg];
    %tvg_curve = 1 - exp(-(range_count * 50) ./ tvg_ref(current_tvg + 1)')';
    range_count = linspace(0, packed_header(idx).range + packed_header(idx).range * 0.2, packed_header(idx).data_count);
    tvg_curve = exp(-(range_count .* 50) ./ tvg_ref(current_tvg + 1)')';
    
    
    range = packed_header(idx).range;
    data_count = packed_header(idx).data_count;
    pulse_length = pwd(idx,:);
    correct_range(idx) = range - (3*range/data_count+pulse_length*1500/4);
    
end


end

