function [amp_values] = getAmpValues()

    amp_values = [];
    amp_values = [amp_values linspace(1.892089844, 4.394531, 11)];
    amp_values(end) = [];
    
    amp_values = [amp_values linspace(4.394531, 10.37598, 11)];
    amp_values(end) = [];
    
	
	amp_values = [amp_values linspace(10.37598, 25.02441, 11)];
	amp_values(end) = [];
    
    

	amp_values = [amp_values linspace(25.02441, 56.15234, 11)];
	amp_values(end) = [];
    
    
	amp_values = [amp_values linspace(56.15234, 105.5908, 11)];
	amp_values(end) = [];
    
    
	amp_values = [amp_values linspace(105.5908, 134.2773, 11)];
	amp_values(end) = [];
    
    
	amp_values = [amp_values linspace(134.2773, 271.2674, 11)];
	
    for idx = length(amp_values)+1:101
        amp_values = [amp_values ((13.6990)/2 * idx) - 177.6633];
        
    end


end

