function [header packed_data] = compareHeaderChange(fid, packed_data, data_ptr)

% packed_data = repmat(...
%     struct(...
%     'data', struct('sensor_data1', [], 'sensor_data2', []), ...
%     'ch_sel', struct('ch_sel1', 0, 'ch_sel2', 0), ...
%     'gain', struct('wd_high_gain', 0, 'wd_low_gain', 0, 'dd_high_gain', 0, 'dd_low_gain', 0)...
%     ),[data_length, 1]);

header = readHeader(fid, data_ptr);

packed_data.data.sensor_data1 = zeros(header.data_count, 1);
packed_data.data.sensor_data2 = zeros(header.data_count, 1);
    
    
end





