function [ est_bottom, current_tag ] = pickCandidate(x, peak_idx, window_size, tag_list, echo_exist, current_burst_number)

tmp_tag = struct(...
    'tag', 0, 'issue', 0, 'score', 0, 'total_rating', 0, 'rating', 0,...
    'min_range', 0, 'max_range', 0, 'point', 0, 'count', 0, 'no_update', 0, 'sleep', 0, 'sleep_count', 0, 'data_count', 0 ...
    );

pwd = fix(window_size/2);
selected_peaks = [];
selected_tags = [];

current_data_count = length(x);

if isempty(tag_list) % 맨 처음 시작
    if ~echo_exist
        current_tag.tag = tmp_tag;
        current_tag.total_rating = 0;
        current_tag.max_tag = 0;
        est_bottom = 0;
        return;
    end
    
    
    tmp_tag = repmat(tmp_tag, [length(peak_idx), 1]);
    current_tag = struct('tag', tmp_tag, 'total_rating', 0, 'max_tag', 0);
    
    for idx = 1:length(peak_idx)
        current_tag.max_tag = idx;
        
        current_tag.tag(idx).tag = idx;
        current_tag.tag(idx).issue = 0;
        current_tag.tag(idx).count = current_tag.tag(idx).count + 1;

        current_tag.tag(idx).point = peak_idx(idx);
        current_tag.tag(idx).max_range = peak_idx(idx) + pwd;
        current_tag.tag(idx).min_range = peak_idx(idx) - pwd;
        current_tag.tag(idx).rating = x(peak_idx(idx));
        current_tag.tag(idx).sleep = 0;
        current_tag.tag(idx).sleep_count = 0;
        current_tag.tag(idx).data_count = current_data_count;
        
        current_tag.tag(idx).total_rating = x(peak_idx(idx));
        current_tag.tag(idx).score = (current_tag.tag(idx).total_rating / current_tag.tag(idx).count...
                                  + current_tag.tag(idx).count / current_burst_number)/2;
        
    end

else
	% 자던 놈 sleep 확인

    for idx = 1:length(tag_list.tag)
        if tag_list.tag(idx).point <= current_data_count
            tag_list.tag(idx).sleep = 0;
            tag_list.tag(idx).sleep_count = 0;
        else
            tag_list.tag(idx).sleep = 1;
            tag_list.tag(idx).sleep_count = tag_list.tag(idx).sleep_count + 1;
            if tag_list.tag(idx).sleep_count > tag_list.tag(idx).count
               tag_list.tag(idx).no_update = tag_list.tag(idx).no_update + 1;
            end
        end
    end
    
    
    tmp_tag = repmat(tmp_tag, [length(peak_idx), 1]);
    current_tag = struct('tag', tmp_tag, 'total_rating', 0, 'max_tag', 0);
    %tag_list = [tag_list; tmp_tag_list];
    
    if ~echo_exist
        prev_issue_idx = find([tag_list.tag(:).issue]);
        if isempty(prev_issue_idx)
            current_tag.tag = tmp_tag;
            current_tag.total_rating = 0;
            current_tag.max_tag = 0;
            est_bottom = 0;
            return;
        else
            est_bottom = tag_list.tag(prev_issue_idx).point;
            current_tag = tag_list;
            return;
        end
    end
    
    current_pos = 1;
    
    for idx=1:length(tag_list.tag)
        for jdx = 1:length(peak_idx)
            if tag_list.tag(idx).min_range < peak_idx(jdx) ...
            && peak_idx(jdx) < tag_list.tag(idx).max_range
               % 지금 핑도 있고 이전 핑에도 있던 피크 가져옴
                current_tag.max_tag = idx;
                current_tag.tag(current_pos).count = tag_list.tag(idx).count + 1;
                current_tag.tag(current_pos).no_update = 0;
                
                current_tag.tag(current_pos).tag = tag_list.tag(idx).tag;
                current_tag.tag(current_pos).issue = 0;
                current_tag.tag(current_pos).point = peak_idx(jdx);
                current_tag.tag(current_pos).max_range = peak_idx(jdx) + pwd;
                current_tag.tag(current_pos).min_range = peak_idx(jdx) - pwd;
                current_tag.tag(current_pos).rating =  x(peak_idx(jdx));
                current_tag.tag(current_pos).sleep = 0; % 지금 나온 피크이므로 무조건 sleep 0
                current_tag.tag(current_pos).sleep_count = 0;
                current_tag.tag(current_pos).data_count = current_data_count;
                
                current_tag.tag(current_pos).total_rating = tag_list.tag(idx).total_rating ...
                                                            + current_tag.tag(current_pos).rating;
                current_tag.tag(current_pos).score = ...
                                    (current_tag.tag(current_pos).total_rating / current_tag.tag(current_pos).count...
                                  + current_tag.tag(current_pos).count / current_burst_number)/2;
                
                selected_peaks = [selected_peaks, peak_idx(jdx)];
                selected_tags = [selected_tags, tag_list.tag(idx).tag];
                current_pos = current_pos + 1;
                break;
            end
        end
    end
    

    
    for idx = 1:length(tag_list.tag)
        flag = 0;
        if tag_list.tag(idx).no_update >= 64 || tag_list.tag(idx).tag == 0
            flag = 1;            
        end
        if flag == 0
            for jdx = 1:length(selected_tags)
                if tag_list.tag(idx).tag == selected_tags(jdx) 
                    % 건너 뜀:위쪽 for문에서 선택한 피크 ||  버린다: 오랫동안 안나오는 피크
                    flag = 1; % 버리는 flag
                    %if tag_list.tag(idx).point > current_data_count % 만약 현재 range가 줄었으면 재워둬
                    %   tag_list.tag(idx).sleep = 1;
                    %   flag = 0;
                    %end
                    break;
                end
            end
        end
        if flag == 0
            % 이번 핑에서는 안 나왔는데 이전 핑에는 있는 피크 보존
            current_tag.tag(current_pos) = tag_list.tag(idx);
            current_tag.tag(current_pos).issue = 0;
            if current_tag.tag(current_pos).sleep == 0
                current_tag.tag(current_pos).no_update = current_tag.tag(current_pos).no_update + 1;
            %else
                %disp('hello');
            end

            current_tag.tag(current_pos).score = ...
                                    (tag_list.tag(idx).total_rating / tag_list.tag(idx).count...
                                    + tag_list.tag(idx).count / current_burst_number)/2;
                              
            current_pos = current_pos + 1;
        end
    end
    current_tag.max_tag = current_pos - 1;
    
    % 이번 핑에서 새롭게 나온 피크
    new_peaks = setdiff(peak_idx(:), selected_peaks, 'stable');
    for jdx=1:length(new_peaks)
        tag_candidate = setdiff(1:max([current_tag.tag(:).tag]), [current_tag.tag(:).tag]);
        if isempty(tag_candidate)
            current_tag.tag(current_pos).tag = current_pos;
        else
            current_tag.tag(current_pos).tag = tag_candidate(1);
        end
        current_tag.tag(current_pos).issue = 0;
        
        current_tag.tag(current_pos).count = current_tag.tag(current_pos).count + 1;

        current_tag.tag(current_pos).point = new_peaks(jdx);
        current_tag.tag(current_pos).max_range = new_peaks(jdx) + pwd;
        current_tag.tag(current_pos).min_range = new_peaks(jdx) - pwd;
        current_tag.tag(current_pos).rating = x(new_peaks(jdx));
        current_tag.tag(current_pos).count = 1;
        current_tag.tag(current_pos).no_update = 0;
        current_tag.tag(current_pos).sleep = 0;
        current_tag.tag(current_pos).sleep_count = 0;
        
        current_tag.tag(current_pos).data_count = current_data_count;
        
        current_tag.tag(current_pos).total_rating = x(new_peaks(jdx));
        current_tag.tag(current_pos).score = (current_tag.tag(current_pos).total_rating / current_tag.tag(current_pos).count...
                                                + current_tag.tag(current_pos).count / current_burst_number)/2;

        current_pos = current_pos + 1;
    end
    
    % 중복 피크 제거 <- 다른 피크를 추적하다가 같은 피크로 합쳐진 경우
    loop_limit = length(current_tag.tag);
    idx = 1;
    while idx < loop_limit
        jdx = idx + 1;
        while jdx < loop_limit
            if current_tag.tag(idx).point == current_tag.tag(jdx).point
                if current_tag.tag(idx).count > current_tag.tag(jdx).count
                    current_tag.tag(jdx) = [];
                else
                    current_tag.tag(idx) = [];
                end
            end
            jdx = jdx + 1;
            loop_limit = length(current_tag.tag);
            
        end
        idx = idx + 1;
    end
    
	current_tag.max_tag = current_pos - 1;

end



% score 매기고 후보 뽑기
sleep_tag_set = find([current_tag.tag(:).sleep]);
search_tag_set = setdiff(1:length(current_tag.tag), sleep_tag_set);
search_tag.tag = current_tag.tag(search_tag_set);
search_tag.total_rating = current_tag.total_rating;
search_tag.max_tag = current_tag.max_tag;
if isempty(search_tag_set)
    est_bottom = 0;
    current_tag = tmp_tag;
    return;
end

[max_val, max_idx] = max([search_tag.tag(:).score]);
if ~isempty(tag_list)
    candidate_idx = max_idx;
    prev_issue_idx = find([tag_list.tag(:).issue]);
    if ~isempty(prev_issue_idx)
        if tag_list.tag(prev_issue_idx).tag ~= search_tag.tag(max_idx).tag % score 1등 변경된 경우
            flag = 0;
            for idx = 1:length(search_tag.tag)
                if search_tag.tag(idx).tag == tag_list.tag(prev_issue_idx).tag
                    now_prev_issue_pos = idx;
                    flag = 1;
                    break;
                end
            end
            if flag == 1 % 이전에 선택된 수심이 지금도 남아있는 경우
                if fix(search_tag.tag(now_prev_issue_pos).count / 2) > search_tag.tag(max_idx).count
                    % 이전에 선택된 수심이 현재 선택된 수심보다 가능성이 높은 경우(후보에서 매우 오래 유지됨)
                    %est_bottom_candidate = tag_list(end).tag(now_prev_issue_pos).point;
                    candidate_idx = now_prev_issue_pos;
                else
                    % 지금 1등이 충분히 후보로 선택될 만큼 여러번 발생한 피크인 경우
                    %est_bottom_candidate = tag_list(end).tag(max_idx).point;
                    candidate_idx = max_idx;
                    %for idx =1:length(current_tag.tag)
                        %if current_tag.tag(idx).tag == search_tag.tag(now_prev_issue_pos).tag
                            %current_tag.tag(idx).issue = 0;
                            %break;
                        %end
                    %end
                end
            else
                % 이전에 선택된 수심이 지금은 없음
                %est_bottom_candidate = tag_list(end).tag(max_idx).point;
                candidate_idx = max_idx;

            end
        else
            candidate_idx = max_idx;

        end
    end
else
    est_bottom = current_tag.tag(max_idx).point - fix(window_size / 2);
    current_tag.tag(max_idx).issue = 1;
    return;
end

% 후보 태그와 매우 인접해있으면서 가장 빠른 태그 뽑음
search_tag.tag(candidate_idx).issue = 1;
est_bottom = search_tag.tag(candidate_idx).point;
for idx = 1:length(search_tag.tag)
    if search_tag.tag(candidate_idx).point - search_tag.tag(idx).min_range > 0 ...
       && search_tag.tag(candidate_idx).point - search_tag.tag(idx).min_range <= pwd
        if est_bottom > search_tag.tag(idx).point  
            est_bottom = search_tag.tag(idx).point;
        end
    end
end

est_bottom = est_bottom - fix(window_size / 2);


for idx = 1:length(current_tag.tag)
    if current_tag.tag(idx).tag == search_tag.tag(candidate_idx).tag
        current_tag.tag(idx).issue = 1;
        break;
    end
end


end

