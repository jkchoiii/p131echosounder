function drawRealTime(data, imag_handler, file_name)

figure;
suptitle(file_name);
plot_handler = [subplot(2,2,1), subplot(2,2,2), subplot(2,2,3), subplot(2,2,4)];

plot(plot_handler(1), data(1).dd_high.data);
plot(plot_handler(2), data(1).dd_low.data);
plot(plot_handler(3), data(1).wd_high.data);
plot(plot_handler(4), data(1).wd_low.data);
data_length = length(data);
for idx = 1:data_length
    
    plot_handler(1).Title.String= ['DD HIGH DATA: ' num2str(idx)];
    set(plot_handler(1).Children, 'Ydata', data(idx).dd_high.data);
    plot_handler(2).Title.String= ['DD LOW DATA: ' num2str(idx)];
    set(plot_handler(2).Children, 'Ydata', data(idx).dd_low.data);
    
    plot_handler(3).Title.String= ['WD HIGH DATA: ' num2str(idx)];
    set(plot_handler(3).Children, 'Ydata', data(idx).wd_high.data);
    plot_handler(4).Title.String= ['WD LOW DATA: ' num2str(idx)];
    set(plot_handler(4).Children, 'Ydata', data(idx).wd_low.data);
    
    
    set(imag_handler(1).Children(1), 'Xdata', imag_handler(1).Children(1).XData + 1);
    set(imag_handler(2).Children(1), 'Xdata', imag_handler(2).Children(1).XData + 1);
    set(imag_handler(3).Children(1), 'Xdata', imag_handler(3).Children(1).XData + 1);
    set(imag_handler(4).Children(1), 'Xdata', imag_handler(4).Children(1).XData + 1);
    
    drawnow;
end
end

