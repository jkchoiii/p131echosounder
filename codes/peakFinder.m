function [ candidate ] = peakFinder(reflected_x, window_size)


peak = [];
x = reflected_x;
peak_threshold_ratio = 0.5;
for idx = 3:length(reflected_x)
    if x(idx) < peak_threshold_ratio % peak thresholds
        continue;
    end
    
    if x(idx - 2) < x(idx - 1) && x(idx-1) >= x(idx)
        if ~isempty(peak)
            %if peak(length(peak))+fix(window_size/2) < idx
                peak = [peak idx-1];
            %end
        else
            peak = [peak idx-1];
        end
        
    end

end

[val, sort_idx] = sort(x(peak), 'descend');
peak_idx = peak(sort_idx);
filter_size = fix(window_size/2);

search_set = peak_idx;
total_removal = [];
current_pos = 1;
while current_pos < length(search_set)
    set_removal = [];
    for search_pos=current_pos+1:length(search_set)
        if search_set(search_pos) <= search_set(current_pos) + filter_size ...
                && search_set(search_pos) >= search_set(current_pos) - filter_size
            set_removal = [set_removal search_set(search_pos)];
            total_removal = [total_removal search_set(search_pos)];
        end
    end
    current_pos = current_pos + 1;
    for rem_idx = 1:length(set_removal)
        search_set(search_set == set_removal(rem_idx)) = [];
    end
    %search_set = setdiff(search_set, set_removal, 'stable');

end

candidate = {search_set};