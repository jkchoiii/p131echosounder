function [est_results] = depthEstimation(gained_data, file_name, file_root)


tx_signal_frequency = [200*1e3 33*1e3]; % 200 + 255 hetero -> 55 and 455 -> only 455

rx_signal_frequency = [(200+255)*1e3 (33+422)*1e3]; % high, low
rx_sampling_frequency = 75*1e3;

T = 1 ./ rx_signal_frequency;
pulse_length = [100*1e-6, 150*1e-6, 300*1e-6, 500*1e-6, 1*1e-3, 1.5*1e-3, 3*1e-3, 5*1e-3;...            % high
                151*1e-6, 303*1e-6, 515*1e-6, 1.06*1e-3 3.03*1e-3, 6.06*1e-3, 10.606*1e-3, 15.15*1e-3]; % low
cycle = fix(pulse_length ./ T');


non_filtered_data = zeros(max([gained_data(:).data_count]), 4, length(gained_data));
filtered_data = zeros(max([gained_data(:).data_count]), 4, length(gained_data));
window_list = zeros(length(gained_data), 1);
pwd_list = zeros(length(gained_data), 4);
est_bottom_list = zeros(length(gained_data), 4) - 1;
data_count = [gained_data(:).data_count]';

draw = 0;

%figure;
textprogressbar('processing data: ');
total_peak_idx = cell(length(gained_data), 4);
tag_list = struct('hist', []);
tag_list = repmat(tag_list, [length(gained_data), 4]);

draw = 1;
for idx = 1:length(gained_data)
    textprogressbar( idx / numel(gained_data)  * 100 );    

    x = [gained_data(idx).wd_high.data, gained_data(idx).wd_low.data, ...
         gained_data(idx).dd_high.data, gained_data(idx).dd_low.data];
   
    pulse_width = [gained_data(idx).wd_high.pwd, gained_data(idx).wd_low.pwd, ...
         gained_data(idx).dd_high.pwd, gained_data(idx).dd_low.pwd];
    valid_idx = find(sum(x));
    
    %figure; plot(x(:,valid_idx));
    for jdx = 1:length(valid_idx)

        
        if (length(valid_idx) == 1) || (length(valid_idx) == 3) % high
            freq_idx = 1;
            
        elseif (length(valid_idx) == 2) || (length(valid_idx) == 4) % low
            freq_idx = 2;
            
        end
        
        tb_signal = abs(toneBurst(rx_sampling_frequency, rx_signal_frequency(freq_idx), cycle(freq_idx, pulse_width(valid_idx)+1)));
        tb_signal = tb_signal ./ max(tb_signal);
        
        current_valid_idx = valid_idx(jdx);
        current_x = x(1:gained_data(idx).data_count, current_valid_idx);
        current_x = current_x ./ max(current_x);
        
        aligned_xcorr = fftshift(xcorr_fft(current_x, tb_signal));
        aligned_xcorr = aligned_xcorr(1:(length(aligned_xcorr)+1)/2);
        aligned_xcorr = aligned_xcorr./max(aligned_xcorr);
        aligned_xcorr(aligned_xcorr<0) = 0;
        %xcorr_x = imresize(xcorr_x, [gained_data(idx).data_count, 1], 'nearest');
        
        window_size = cycle(freq_idx, pulse_width(current_valid_idx));
        
        window_list(idx) = window_size;
        pwd_list(idx,:) = pulse_width;
        
        b = (1/window_size)*ones(1,window_size);
        a = 1;
        
        smooth_x = filter(b, a, aligned_xcorr);
        smooth_x = smooth_x./max(smooth_x);

        [reflected_x, max_idx, echo_exist] = removeTransmitSource(smooth_x, window_size, draw);
        reflected_x = reflected_x./max(reflected_x);
        %reflected_x = smooth_x;
        
        non_filtered_data(1:gained_data(idx).data_count,current_valid_idx, idx) = current_x;
        filtered_data(1:gained_data(idx).data_count,current_valid_idx, idx) = reflected_x;
        
        [ peak_idx ] = peakFinder(reflected_x, window_size);
        total_peak_idx(idx, current_valid_idx) = peak_idx;
        
        if idx == 3190
           disp('hi') 
        end
        
        
        if idx == 1
            [ est_bottom, current_tag ] = pickCandidate(reflected_x, peak_idx{:},...
                                                    window_size, [], echo_exist, idx);
        else
            find_flag = 0;
            if idx > 64
                find_max = 64;
            else
                find_max = idx-1;
            end
            for find_idx = 1:find_max
                if est_bottom_list(idx - find_idx, current_valid_idx) ~= -1
                    unified_idx = find_idx;
                    unified_jdx = current_valid_idx;
                    [ est_bottom, current_tag ] = pickCandidate(reflected_x, peak_idx{:},...
                                          window_size, tag_list(idx-unified_idx, unified_jdx).hist, echo_exist, idx);
                    find_flag = 1;
                    break;
                else
                    if mod(current_valid_idx, 2) == 1 % Ȧ��
                        if est_bottom_list(idx - find_idx, current_valid_idx + 1) ~= -1
                            unified_idx = find_idx;
                            unified_jdx = current_valid_idx + 1;
                            [ est_bottom, current_tag ] = pickCandidate(reflected_x, peak_idx{:},...
                                          window_size, tag_list(idx-unified_idx, unified_jdx).hist, echo_exist, idx);
                    
                            find_flag = 1;
                            
                        end
                    else % ¦��
                        if est_bottom_list(idx - find_idx, current_valid_idx - 1) ~= -1
                            unified_idx = find_idx;
                            unified_jdx = current_valid_idx - 1;
                            [ est_bottom, current_tag ] = pickCandidate(reflected_x, peak_idx{:},...
                                          window_size, tag_list(idx-unified_idx, unified_jdx).hist, echo_exist, idx);
                    
                            find_flag = 1;
                            
                        end
                    end
                end
            end
            if find_flag == 0
                [ est_bottom, current_tag ] = pickCandidate(reflected_x, peak_idx{:},...
                                          window_size, [], echo_exist, idx);
            end
            
        end
        tag_list(idx, current_valid_idx).hist = current_tag;
        
        %tag_list{idx, valid_idx} = current_tag(end);
        est_bottom_list(idx,current_valid_idx) = est_bottom;

    end
    
end
textprogressbar(' done');

plotx = linspace(1, length(est_bottom_list), length(est_bottom_list)); 
figure;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.94]);
suptitle(file_name);
imag_handler = [subplot(2,2,1), subplot(2,2,2), subplot(2,2,3), subplot(2,2,4)];


imagesc(imag_handler(1), squeeze(non_filtered_data(:,3,:))); 
imag_handler(1).Title.String= 'dd high';
imagesc(imag_handler(2), squeeze(non_filtered_data(:,4,:))); 
imag_handler(2).Title.String= 'dd low';
imagesc(imag_handler(3), squeeze(non_filtered_data(:,1,:))); 
imag_handler(3).Title.String= 'wd high';
imagesc(imag_handler(4), squeeze(non_filtered_data(:,2,:))); 
imag_handler(4).Title.String= 'wd low';


saveas(gcf, fullfile(file_root, 'img', [file_name '_raw_data.png']));

plot_pts=find(est_bottom_list(:,3)~=-1); 
hold(imag_handler(1), 'on');
scatter(imag_handler(1), plotx(plot_pts), est_bottom_list(plot_pts,3), 'r*')    

hold(imag_handler(2), 'on');
plot_pts=find(est_bottom_list(:,4)~=-1); 
scatter(imag_handler(2), plotx(plot_pts), est_bottom_list(plot_pts,4), 'r*')  

hold(imag_handler(3), 'on');
plot_pts=find(est_bottom_list(:,1)~=-1); 
scatter(imag_handler(3), plotx(plot_pts), est_bottom_list(plot_pts,1), 'r*')  

hold(imag_handler(4), 'on');
plot_pts=find(est_bottom_list(:,2)~=-1); 
scatter(imag_handler(4), plotx(plot_pts), est_bottom_list(plot_pts,2), 'r*')  


saveas(gcf, fullfile(file_root, 'img', [file_name '_raw_data2.png']));

close(gcf)
% figure;
% sq_nf_data = squeeze(non_filtered_data(:,1,:));
% sq_data = squeeze(filtered_data(:,1,:));
% for idx = 5900:length(filtered_data)
%     subplot(2,1,1)
%     plot(sq_nf_data(:,idx));
%     drawnow
%     subplot(2,1,2)
%     plot(sq_data(:,idx))
%     drawnow
% end


% sum_filt = squeeze(sum(filtered_data, 2));
% sum_filt(sum_filt < 0) = 0;
% sum_nonfilt = squeeze(sum(non_filtered_data, 2));
% sum_nonfilt(sum_nonfilt < 0) = 0;
% 
% 
% [y, x] = max(sum_filt);
% plot_x = linspace(1, size(sum_filt, 2), size(sum_filt, 2));
% plot_y = x - fix(window_size)/2;
% plot_y(plot_y<0) = 0;
% 
% figure; imagesc(sum_filt);hold on; scatter(plot_x, plot_y, 'r*');
% colorbar(); title(file_name);


%est_results = sum_filt;
est_results = [];
% 
% figure;
% 
% for idx = 1:size(sum_filt, 2)
%     
%     plot(sum_filt(:,idx));
%     %hold on;
%     %scatter(x(idx), y(idx), 'r*');
%     %hold off;
%     title(num2str(idx));
%     pause(0.05)
%     
% end



end

