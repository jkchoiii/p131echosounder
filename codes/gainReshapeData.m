function [ result_imag ] = gainReshapeData(gained_data)

%%

total_imag = zeros(length(gained_data(1).wd_high.data), length(gained_data), 4);

total_imag(:,:,1) = reshape(extractfield([gained_data(:).dd_high], 'data'), [size(total_imag, 1), size(total_imag, 2)] );
total_imag(:,:,2) = reshape(extractfield([gained_data(:).dd_low], 'data'), [size(total_imag, 1), size(total_imag, 2)] );
total_imag(:,:,3) = reshape(extractfield([gained_data(:).wd_high], 'data'), [size(total_imag, 1), size(total_imag, 2)] );
total_imag(:,:,4) = reshape(extractfield([gained_data(:).wd_low], 'data'), [size(total_imag, 1), size(total_imag, 2)] );

a = imresize(total_imag(:,:,1), 0.25);
b = imresize(total_imag(:,:,2), 0.25);
c = imresize(total_imag(:,:,3), 0.25);
d = imresize(total_imag(:,:,4), 0.25);

result_imag = zeros(size(a, 1), size(a, 2), 4);

result_imag(:,:,1) = a;
result_imag(:,:,2) = b;
result_imag(:,:,3) = c;
result_imag(:,:,4) = d;

end

