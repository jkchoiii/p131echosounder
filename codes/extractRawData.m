function extractRawData(packed_data, file_name)

data_length = length(packed_data(1).data.sensor_data1);

data = zeros(data_length, length(packed_data), 2);

data(:,:,1) = reshape(extractfield([packed_data(:).data], 'sensor_data1'), [data_length, length(packed_data)]);
data(:,:,2) = reshape(extractfield([packed_data(:).data], 'sensor_data2'), [data_length, length(packed_data)]);
data = data ./max(max(data));

window_size = 5;
sum_data = zeros(length(packed_data), size(data, 1)/window_size, size(data, 3));
total_non_data = zeros(size(sum_data, 2), 1);

for jdx = 1:size(data,3)
    for idx = 1:length(packed_data)
        sum_data(idx,:, jdx) = sum(reshape(data(:,idx,jdx), ...
            [window_size, size(data, 1) / window_size]));

    end
    total_non_data(jdx) = sum(sum(data(:,:,jdx)) == 0); % 이빨빠진 수
end
sum_data = sum_data ./ window_size;
window_count = 10;
movsum_data = movsum(sum_data, window_count , 2);
movsum_data = movsum_data ./ max(max(movsum_data));
movsum_data = permute(movsum_data, [2,1,3]);
movsum_data = movsum_data(101:end,:,:); % 발진성

[~, max_idx] = max(movsum_data);
max_idx = squeeze(max_idx);


sup_h = figure;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.94]);

subplot(3,2,1);
imagesc(data(:,:,1)); colormap('jet')
title(['raw data: ch1, non data count: ' num2str(total_non_data(1))]);

subplot(3,2,2);
imagesc(data(:,:,2)); colormap('jet')
title(['raw data: ch2, non data count: ' num2str(total_non_data(2))]);

subplot(3,2,3);
imagesc(movsum_data(:,:,1)); colormap('jet')%colormap('gray')
%hold on; scatter(linspace(0, size(movsum_data(:,:,1), 2), length(max_idx(:,1))), max_idx(:,1), 'r', 'LineWidth', 1);
title(['moving sum result: ch1, window size: ' num2str(window_size * window_count) ' stride: ' num2str(window_size)])

subplot(3,2,4);
imagesc(movsum_data(:,:,2)); colormap('jet')%colormap('gray')
%hold on; scatter(linspace(0, size(movsum_data(:,:,2), 2), length(max_idx(:,2))), max_idx(:,2), 'r', 'LineWidth', 1);
title(['moving sum result: ch2, window size: ' num2str(window_size * window_count) ' stride: ' num2str(window_size)])

h=subplot(3, 2, 5);
scatter(linspace(0, size(movsum_data(:,:,1), 2), length(max_idx(:,1))), max_idx(:,1), 'r', 'LineWidth', 1);
ylim([0 length(max_idx)]);
xlim([0 size(movsum_data(:,:,2), 2)]);
set(h,'YDir','reverse')


title('estimaton result: ch1')

h=subplot(3, 2, 6);
scatter(linspace(0, size(movsum_data(:,:,2), 2), length(max_idx(:,2))), max_idx(:,2), 'r', 'LineWidth', 1);
ylim([0 length(max_idx)]);
xlim([0 size(movsum_data(:,:,2), 2)]);
set(h,'YDir','reverse')

title('estimaton result: ch2')

suptitle(file_name);

saveas(gcf, [file_name '.png']);

%imagesc(movsum_data); colormap('jet')
%hold on; scatter(linspace(0, size(movsum_data, 2), length(max_idx)), max_idx, 'r');
%test_data = zeros(size(movsum_data, 1), size(movsum_data,2));
%test_data = movsum_data;
%test_data(test_data < 0.5) = 0;

%figure; imagesc(test_data'); colormap('jet')
%figure; imagesc(data(:,:,1)); colormap('jet')
%figure; imagesc(sum_data'); colormap('jet')
%figure; imagesc(movsum_data'); colormap('jet')



% 
% sum_idx = zeros(length(packed_data), 1);
% for idx = 1:length(packed_header)
%     
%     [~, max_point] = max(sum_data(idx,60:end));
%     sum_idx(idx) = max_point + 60;
%     
% end
% 
% sum_data = sum_data - mean(sum_data, 2);
% sum_data(sum_data < max(sum_data) * 0.4) = 0;
% figure; imagesc(sum_data)
% figure;
% for idx = 1:length(packed_header)
%     ypoint = linspace(0, max(sum_data(idx,:)), length(packed_header));
%     xpoint = ones(length(packed_header), 1) .* sum_idx(idx);
%     plot(sum_data(idx, :));
%     hold on; plot(xpoint, ypoint, 'r', 'LineWidth', 2);
%     hold off;
%     %scatter()
%     pause(0.1)
% end

end
